Imported the tables by using the import wizard in MySQL 

Did not change any of the datatypes but made alterations to the data labels to make sure that the queries being ran where easier to understand 

Used the date_format to convert the date string into the acquired format for the question and ensured that the output for the range was in 
two decimal places by using the format function in MySQL. 