DELIMITER //
Create procedure Jamie_Challenge2()
BEGIN




create temporary table temp_tbl(

`date` text,
`Range` varchar(50)
);
 
insert into temp_tbl (`date`, `Range`)
SELECT DISTINCT date, ABS(open - close) AS `Range`
FROM sample_dataset3
ORDER BY `Range` DESC
LIMIT 3;

create temporary table temp_tbl2( 

`max` varchar(50),
`date` text

);

insert into temp_tbl2 (`max`, `date`)
	Select max(high), `date`
	From sample_dataset3
	Where `date` In (Select `date` From temp_tbl)
	Group by `date`;

create temporary table temp_tbl3( 
`date` text,
`time` text
);

insert into temp_tbl3 (`date`, `time`)
Select a.`date`, date_format(str_to_date(time, "%H%i"), "%H:%i") as maxHigh
From sample_dataset3 as a INNER join temp_tbl2 as b
On a.`date` = b.`date` and a.high = b.max;

Create temporary table final_table(
`Date` text, 
`Range` varchar(50),
`Time_Max` text
);

insert into final_table(`Date`,`Range`, `Time_Max`)
SELECT 
    B.`date` AS `Date`,
    FORMAT(A.`Range`, 2) AS `Range`,
    B.time AS `Time Max`
FROM
    temp_tbl AS A
        INNER JOIN
    temp_tbl3 AS B ON A.`date` = B.`date`;
 
 Select * from final_table;
 
drop table temp_tbl;
drop table temp_tbl2;
drop table temp_tbl3;
drop table final_table;

 End //
Delimiter ;


call Jamie_Challenge2();