DELIMITER //

Create procedure Jamie_Challenge1 ()
Begin


SELECT ticker,
SUM(vol*close)/SUM(vol) as VWAP,
date_format('2010-10-11 09:00:00', '%d/%m/%Y') as date,
date_format('2010-10-11 09:00:00', '%H:%i') as start,
date_format(date_add('2010-10-11 09:00:00', interval 5 hour), '%H:%i') as endtime
from sample_dataset2
where str_to_date(`date`, '%Y%m%d%H%i') between str_to_date('2010-10-11 09:00', '%Y-%m-%d %H:%i') and date_add(str_to_date('2010-10-11 09:00', '%Y-%m-%d %H:%i'), interval 5 hour);


end // 

DELIMITER ;

call Jamie_Challenge1;